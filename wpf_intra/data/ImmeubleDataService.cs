﻿using System.Collections.Generic;
using wpf_model;

namespace wpf_intra.data
{
    class ImmeubleDataService : IDataService<Immeuble>
    {
        static List<Immeuble> immeubles;

        public ImmeubleDataService()
        {
            immeubles = new List<Immeuble>
            {
                new Immeuble {Adresse = "ave Du Collège", CodePostal = "G9N 6V8", NoCivique = "2663", Ville="Shawinigan" },
                new Immeuble {Adresse = "boul. St-Sacrement", CodePostal = "G9N 3M8", NoCivique = "1100", Ville="Shawinigan"}
            };
        }

        public IEnumerable<Immeuble> GetAll()
        {
            foreach (Immeuble i in immeubles)
            {
                yield return i;
            }
        }
    }
}
