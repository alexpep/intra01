﻿using System.Collections.Generic;

namespace wpf_intra.data
{
    interface IDataService<T>
    {
        IEnumerable<T> GetAll();
    }
}
