﻿using System.Windows;

namespace wpf_intra
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainViewModel mainViewModel;

        public MainWindow()
        {
            InitializeComponent();

            /// TODO : Il manque de quoi ici! creer le mainviewmodel + datacontext
            /// 
            MainViewModel mainViewModel = new MainViewModel();
            DataContext = mainViewModel;
            
        }
    }
}
