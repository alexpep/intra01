﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using wpf_intra.data;
using wpf_model;

namespace wpf_intra
{
    class MainViewModel : BaseViewModel
    {
        private ImmeubleDataService _immeubleDataService;
        public ObservableCollection<Immeuble> Immeubles { get; set; }
        public Immeuble _selectedImmeuble;
        public ICommand OpenGoogleMapCommand { get; set; }
        
        public MainViewModel()
        {
            _immeubleDataService = new ImmeubleDataService();
            Immeubles = new ObservableCollection<Immeuble>();

            OpenGoogleMapCommand = new DelegateCommand(OpenGoogleMap, CanOpenGoogleMap);

            Load();

        }

        public Immeuble SelectedImmeuble {
            get
            {
                return _selectedImmeuble;
            }
            set
            {
                _selectedImmeuble = value;
                OnPropertyChanged();
            }
        }

        private void Load()
        {
            /// TODO : Il manque de quoi ici! 
            /// 
            var immeubles = _immeubleDataService.GetAll();
            Immeubles.Clear();

            foreach (var immeuble in immeubles)
            {
                immeuble.Query = immeuble.NoCivique+'+'+immeuble.Adresse+ '+'+immeuble.Ville+ '+'+immeuble.CodePostal;
                Immeubles.Add(immeuble);
            }


        }

        public void OpenGoogleMap(object param)
        {
            /// TODO : Il manque de quoi ici! *** pas oublier d'aller commandparam binding
            /// 
            
            System.Diagnostics.Process.Start("https://www.google.com/maps/search/?api=1&query="+param);

        }

        private bool CanOpenGoogleMap(object param)
        {
            return true;
        }
    

    }
        
}

