﻿namespace wpf_model
{
    public class Immeuble
    {
        public int Id { get; set; }

        public string NoCivique { get; set; }

        public string Adresse { get; set; }

        public string Appartement { get; set; }

        public string CodePostal { get; set; }
        public string Ville { get; set; }

        public string CompletUneLigne => NoCivique + " " + Adresse + " " + Appartement + ", " + Ville;

        /// TODO : Il manque de quoi ici! add query
        /// 
        public string Query { get; set; }

    }
}
